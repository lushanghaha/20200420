-- apply type
-- 圓榮
INSERT INTO public.apply_type (id, applytype, applytypename, updatetime, datarole_1, datarole_2, company_id) VALUES (15, 'O', '門診', '2020-02-18 22:13:41.000000', null, null, 13373702);
INSERT INTO public.apply_type (id, applytype, applytypename, updatetime, datarole_1, datarole_2, company_id) VALUES (16, 'E', '急診', '2020-02-18 12:43:14.000000', null, null, 13373702);
INSERT INTO public.apply_type (id, applytype, applytypename, updatetime, datarole_1, datarole_2, company_id) VALUES (17, 'H', '住院', '2020-02-18 18:37:07.000000', null, null, 13373702);
-- 員生
INSERT INTO public.apply_type (id, applytype, applytypename, updatetime, datarole_1, datarole_2, company_id) VALUES (18, 'O', '門診', '2020-02-18 22:13:41.000000', null, null, 13373703);
INSERT INTO public.apply_type (id, applytype, applytypename, updatetime, datarole_1, datarole_2, company_id) VALUES (19, 'E', '急診', '2020-02-18 12:43:14.000000', null, null, 13373703);
INSERT INTO public.apply_type (id, applytype, applytypename, updatetime, datarole_1, datarole_2, company_id) VALUES (20, 'H', '住院', '2020-02-18 18:37:07.000000', null, null, 13373703);

-- division
-- 圓榮
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030235, '01', '內科', '01', '家醫科', '家醫科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030236, '01', '內科', '12', '神經內科', '神經內科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030237, '01', '內科', 'AA', '胃腸肝膽科', '胃腸肝膽科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030238, '01', '內科', 'AB', '心臟內科', '心臟內科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030239, '01', '內科', 'AC', '胸腔內科', '胸腔內科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030240, '01', '內科', 'AD', '腎臟內科', '腎臟內科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030241, '01', '內科', 'AG', '新陳代謝科', '新陳代謝科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030242, '02', '外科', '03', '外科', '外科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030243, '02', '外科', '07', '神經外科', '神經外科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030244, '02', '外科', 'BA', '直腸外科', '直腸外科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030245, '02', '外科', 'BB', '心臟外科', '心臟外科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030246, '02', '外科', 'BC', '胸腔外科', '胸腔外科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030247, '02', '外科', '08', '泌尿外科', '泌尿外科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030248, '03', '骨科', '06', '骨科', '骨科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030249, '04', '婦產科', '05', '婦產科', '婦產科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030250, '06', '其他科', '13', '身心科', '身心科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030251, '06', '其他科', '14', '復健科', '復健科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030252, '06', '其他科', '15', '整型外科', '整型外科', null, null, 13373702);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030253, '05', '急診', 'ER', '急診科', '急診科', null, null, 13373702);
-- 員生
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030209, '01', '內科', '01', '家醫科', '家醫科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030210, '01', '內科', '12', '神經內科', '神經內科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030211, '01', '內科', 'AA', '胃腸肝膽科', '胃腸肝膽科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030212, '01', '內科', 'AB', '心臟內科', '心臟內科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030213, '01', '內科', 'AC', '胸腔內科', '胸腔內科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030214, '01', '內科', 'AD', '腎臟內科', '腎臟內科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030215, '01', '內科', 'AG', '新陳代謝科', '新陳代謝科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030216, '01', '內科', '04', '小兒科', '小兒科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030217, '02', '外科', '03', '外科', '外科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030218, '02', '外科', '07', '神經外科', '神經外科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030219, '02', '外科', 'BA', '直腸外科', '直腸外科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030220, '02', '外科', 'BB', '心臟外科', '心臟外科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030221, '02', '外科', 'BC', '胸腔外科', '胸腔外科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030222, '02', '外科', '08', '泌尿外科', '泌尿外科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030223, '02', '外科', '01A', '高壓氧科', '高壓氧科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030224, '02', '外科', '01B', '傷口治療科', '傷口治療科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030225, '03', '骨科', '06', '骨科', '骨科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030226, '04', '婦產科', '05', '婦產科', '婦產科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030228, '06', '其他科', '09', '耳鼻喉科', '耳鼻喉科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030229, '06', '其他科', '13', '身心科', '身心科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030230, '06', '其他科', '14', '復健科', '復健科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030231, '06', '其他科', '15', '整型外科', '整型外科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030232, '06', '其他科', '60', '中醫', '中醫', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030233, '06', '其他科', 'AE', '風濕免疫科', '風濕免疫科', null, null, 13373703);
INSERT INTO public.division (id, jhi_type, typename, divisionid, divisionname, description, datarole_1, datarole_2, company_id) VALUES (35030234, '05', '急診', 'ER', '急診科', '急診科', null, null, 13373703);

-- division type
-- 圓榮
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093529, '01', '內科', '內科', null, null, 13373702);
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093530, '02', '外科', '外科', null, null, 13373702);
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093533, '06', '其他科', '其他科', null, null, 13373702);
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093531, '04', '婦產科', '婦產科', null, null, 13373702);
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093532, '03', '骨科', '骨科', null, null, 13373702);
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093534, '05', '急診', '急診', null, null, 13373702);
-- 員生
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093539, '05', '急診', '急診', null, null, 13373703);
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093538, '04', '婦產科', '婦產科', null, null, 13373703);
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093537, '03', '骨科', '骨科', null, null, 13373703);
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093535, '01', '內科', '內科', null, null, 13373703);
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093536, '02', '外科', '外科', null, null, 13373703);
INSERT INTO public.division_type (id, type, typename, description, datarole_1, datarole_2, company_id) VALUES (11093540, '06', '其他科', '其他科', null, null, 13373703);

-- file group
-- 圓榮
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (19, '60237057_GO1', 1, '2020-02-19 07:38:28.000000', null, null, 15);
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (20, '60237057_GO2', 0, '2020-02-19 00:04:48.000000', null, null, 15);
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (21, '60237057_GE1', 1, '2020-02-19 02:24:32.000000', null, null, 16);
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (22, '60237057_GE2', 0, '2020-02-19 02:24:32.000000', null, null, 16);
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (23, '60237057_GH1', 1, '2020-02-19 02:24:32.000000', null, null, 17);
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (24, '60237057_GH2', 0, '2020-02-19 02:24:32.000000', null, null, 17);
-- 員生
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (25, '80892974_GO1', 1, '2020-02-19 07:38:28.000000', null, null, 18);
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (26, '80892974_GO2', 0, '2020-02-19 00:04:48.000000', null, null, 18);
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (27, '80892974_GE1', 1, '2020-02-19 02:24:32.000000', null, null, 19);
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (28, '80892974_GE2', 0, '2020-02-19 02:24:32.000000', null, null, 19);
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (29, '80892974_GH1', 1, '2020-02-19 02:24:32.000000', null, null, 20);
INSERT INTO public.file_group (id, grouptype, requirednum, updatetime, datarole_1, datarole_2, apply_type_id) VALUES (30, '80892974_GH2', 0, '2020-02-19 02:24:32.000000', null, null, 20);

-- file type
-- 圓榮
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (29, 'B', '就醫資料', '', '2020-02-18 07:39:45.000000', null, null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (38, 'L', '數位服務同意書', '', '2020-02-18 07:39:45.000000', null, null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (39, 'M', '醫院授權範圍同意聲明', '', '2020-02-18 07:39:45.000000', null, null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (40, 'Z', '綜合文件', '', '2020-02-18 07:39:45.000000', null, null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (32, 'E', '病理切片報告', '若您是首次申請防癌險、重大疾病險請選此項', '2020-02-18 07:39:45.000000', '07', null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (34, 'G', '門診紀錄單', '若您沒有向醫院申請開立診斷證明書請選此項', '2020-02-18 07:39:45.000000', '02', null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (30, 'C', '出院病摘', '若您沒有向醫院申請開立診斷證明書請選此項', '2020-02-18 07:39:45.000000', '04', null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (31, 'D', '費用明細', '若您需申請實支實付醫療險請選此項', '2020-02-18 07:39:45.000000', '05', null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (36, 'I', '急診紀錄單', '若您沒有向醫院申請開立診斷證明書請選此項', '2020-02-18 07:39:45.000000', '03', null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (33, 'F', '放射影像檢查報告', '若您需申請骨折但未住院請選此項', '2020-02-18 07:39:45.000000', '06', null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (37, 'J', '手術紀錄', '非必選', '2020-02-18 07:39:45.000000', '08', null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (28, 'A', '診斷證明書', '若您已向醫院申請開立診斷證明書請選此項', '2020-02-18 07:39:45.000000', '01', null, 13373703);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (35, 'H', '癌症化療治療紀錄', '若您需申請防癌險請選此項', '2020-02-18 07:39:45.000000', '09', null, 13373703);
-- 員生
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (41, 'B', '就醫資料', '', '2020-02-18 07:39:45.000000', null, null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (42, 'L', '數位服務同意書', '', '2020-02-18 07:39:45.000000', null, null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (43, 'M', '醫院授權範圍同意聲明', '', '2020-02-18 07:39:45.000000', null, null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (44, 'Z', '綜合文件', '', '2020-02-18 07:39:45.000000', null, null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (45, 'E', '病理切片報告', '若您是首次申請防癌險、重大疾病險請選此項', '2020-02-18 07:39:45.000000', '07', null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (46, 'G', '門診紀錄單', '若您沒有向醫院申請開立診斷證明書請選此項', '2020-02-18 07:39:45.000000', '02', null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (47, 'C', '出院病摘', '若您沒有向醫院申請開立診斷證明書請選此項', '2020-02-18 07:39:45.000000', '04', null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (48, 'D', '費用明細', '若您需申請實支實付醫療險請選此項', '2020-02-18 07:39:45.000000', '05', null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (49, 'I', '急診紀錄單', '若您沒有向醫院申請開立診斷證明書請選此項', '2020-02-18 07:39:45.000000', '03', null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (50, 'F', '放射影像檢查報告', '若您需申請骨折但未住院請選此項', '2020-02-18 07:39:45.000000', '06', null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (51, 'J', '手術紀錄', '非必選', '2020-02-18 07:39:45.000000', '08', null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (52, 'A', '診斷證明書', '若您已向醫院申請開立診斷證明書請選此項', '2020-02-18 07:39:45.000000', '01', null, 13373702);
INSERT INTO public.file_type (id, filetype, filetypename, description, updatetime, datarole_1, datarole_2, company_id) VALUES (53, 'H', '癌症化療治療紀錄', '若您需申請防癌險請選此項', '2020-02-18 07:39:45.000000', '09', null, 13373702);

-- file type group rel
-- 圓榮
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (80, '2020-02-19 05:42:06.000000', null, null, 28, 19);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (81, '2020-02-19 05:42:06.000000', null, null, 34, 19);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (82, '2020-02-19 05:42:06.000000', null, null, 31, 20);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (83, '2020-02-19 05:42:06.000000', null, null, 33, 20);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (84, '2020-02-19 05:42:06.000000', null, null, 32, 20);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (85, '2020-02-19 05:42:06.000000', null, null, 37, 20);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (86, '2020-02-19 05:42:06.000000', null, null, 35, 20);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (87, '2020-02-19 05:42:06.000000', null, null, 28, 21);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (88, '2020-02-19 05:42:06.000000', null, null, 36, 21);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (89, '2020-02-19 05:42:06.000000', null, null, 31, 22);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (90, '2020-02-19 05:42:06.000000', null, null, 33, 22);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (91, '2020-02-19 05:42:06.000000', null, null, 32, 22);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (92, '2020-02-19 05:42:06.000000', null, null, 37, 22);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (93, '2020-02-19 05:42:06.000000', null, null, 28, 23);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (94, '2020-02-19 05:42:06.000000', null, null, 30, 23);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (95, '2020-02-19 05:42:06.000000', null, null, 31, 24);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (96, '2020-02-19 05:42:06.000000', null, null, 33, 24);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (97, '2020-02-19 05:42:06.000000', null, null, 32, 24);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (98, '2020-02-19 05:42:06.000000', null, null, 37, 24);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (99, '2020-02-19 05:42:06.000000', null, null, 35, 24);
-- 員生
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (100, '2020-02-19 05:42:06.000000', null, null, 28, 25);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (101, '2020-02-19 05:42:06.000000', null, null, 34, 25);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (102, '2020-02-19 05:42:06.000000', null, null, 31, 26);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (103, '2020-02-19 05:42:06.000000', null, null, 33, 26);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (104, '2020-02-19 05:42:06.000000', null, null, 32, 26);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (105, '2020-02-19 05:42:06.000000', null, null, 37, 26);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (106, '2020-02-19 05:42:06.000000', null, null, 35, 26);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (107, '2020-02-19 05:42:06.000000', null, null, 28, 27);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (108, '2020-02-19 05:42:06.000000', null, null, 36, 27);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (109, '2020-02-19 05:42:06.000000', null, null, 31, 28);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (110, '2020-02-19 05:42:06.000000', null, null, 33, 28);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (111, '2020-02-19 05:42:06.000000', null, null, 32, 28);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (112, '2020-02-19 05:42:06.000000', null, null, 37, 28);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (113, '2020-02-19 05:42:06.000000', null, null, 28, 29);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (114, '2020-02-19 05:42:06.000000', null, null, 30, 29);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (115, '2020-02-19 05:42:06.000000', null, null, 31, 30);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (116, '2020-02-19 05:42:06.000000', null, null, 33, 30);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (117, '2020-02-19 05:42:06.000000', null, null, 32, 30);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (118, '2020-02-19 05:42:06.000000', null, null, 37, 30);
INSERT INTO public.file_type_group_rel (id, updatetime, datarole_1, datarole_2, file_type_id, file_group_id) VALUES (119, '2020-02-19 05:42:06.000000', null, null, 35, 30);

-- apply type division rel
-- 圓榮
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (307, '2020-02-19 03:22:26.000000', null, null, 35030235, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (308, '2020-02-19 03:22:26.000000', null, null, 35030236, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (309, '2020-02-19 03:22:26.000000', null, null, 35030237, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (310, '2020-02-19 03:22:26.000000', null, null, 35030238, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (311, '2020-02-19 03:22:26.000000', null, null, 35030239, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (312, '2020-02-19 03:22:26.000000', null, null, 35030240, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (313, '2020-02-19 03:22:26.000000', null, null, 35030241, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (314, '2020-02-19 03:22:26.000000', null, null, 35030242, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (315, '2020-02-19 03:22:26.000000', null, null, 35030243, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (316, '2020-02-19 03:22:26.000000', null, null, 35030244, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (317, '2020-02-19 03:22:26.000000', null, null, 35030245, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (318, '2020-02-19 03:22:26.000000', null, null, 35030246, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (319, '2020-02-19 03:22:26.000000', null, null, 35030247, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (320, '2020-02-19 03:22:26.000000', null, null, 35030248, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (321, '2020-02-19 03:22:26.000000', null, null, 35030249, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (322, '2020-02-19 03:22:26.000000', null, null, 35030250, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (323, '2020-02-19 03:22:26.000000', null, null, 35030251, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (324, '2020-02-19 03:22:26.000000', null, null, 35030252, 15);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (343, '2020-02-19 03:22:26.000000', null, null, 35030253, 16);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (325, '2020-02-19 03:22:26.000000', null, null, 35030235, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (326, '2020-02-19 03:22:26.000000', null, null, 35030236, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (327, '2020-02-19 03:22:26.000000', null, null, 35030237, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (328, '2020-02-19 03:22:26.000000', null, null, 35030238, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (329, '2020-02-19 03:22:26.000000', null, null, 35030239, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (330, '2020-02-19 03:22:26.000000', null, null, 35030240, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (331, '2020-02-19 03:22:26.000000', null, null, 35030241, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (332, '2020-02-19 03:22:26.000000', null, null, 35030242, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (333, '2020-02-19 03:22:26.000000', null, null, 35030243, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (334, '2020-02-19 03:22:26.000000', null, null, 35030244, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (335, '2020-02-19 03:22:26.000000', null, null, 35030245, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (336, '2020-02-19 03:22:26.000000', null, null, 35030246, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (337, '2020-02-19 03:22:26.000000', null, null, 35030247, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (338, '2020-02-19 03:22:26.000000', null, null, 35030248, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (339, '2020-02-19 03:22:26.000000', null, null, 35030249, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (340, '2020-02-19 03:22:26.000000', null, null, 35030250, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (341, '2020-02-19 03:22:26.000000', null, null, 35030251, 17);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (342, '2020-02-19 03:22:26.000000', null, null, 35030252, 17);
-- 員生
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (256, '2020-02-19 03:22:26.000000', null, null, 35030209, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (257, '2020-02-19 03:22:26.000000', null, null, 35030210, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (258, '2020-02-19 03:22:26.000000', null, null, 35030211, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (259, '2020-02-19 03:22:26.000000', null, null, 35030212, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (260, '2020-02-19 03:22:26.000000', null, null, 35030213, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (261, '2020-02-19 03:22:26.000000', null, null, 35030214, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (262, '2020-02-19 03:22:26.000000', null, null, 35030215, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (263, '2020-02-19 03:22:26.000000', null, null, 35030216, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (264, '2020-02-19 03:22:26.000000', null, null, 35030217, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (265, '2020-02-19 03:22:26.000000', null, null, 35030218, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (266, '2020-02-19 03:22:26.000000', null, null, 35030219, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (267, '2020-02-19 03:22:26.000000', null, null, 35030220, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (268, '2020-02-19 03:22:26.000000', null, null, 35030221, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (269, '2020-02-19 03:22:26.000000', null, null, 35030222, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (270, '2020-02-19 03:22:26.000000', null, null, 35030223, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (271, '2020-02-19 03:22:26.000000', null, null, 35030224, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (272, '2020-02-19 03:22:26.000000', null, null, 35030225, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (273, '2020-02-19 03:22:26.000000', null, null, 35030226, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (275, '2020-02-19 03:22:26.000000', null, null, 35030228, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (276, '2020-02-19 03:22:26.000000', null, null, 35030229, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (277, '2020-02-19 03:22:26.000000', null, null, 35030230, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (278, '2020-02-19 03:22:26.000000', null, null, 35030231, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (279, '2020-02-19 03:22:26.000000', null, null, 35030232, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (280, '2020-02-19 03:22:26.000000', null, null, 35030233, 18);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (306, '2020-02-19 03:22:26.000000', null, null, 35030234, 19);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (281, '2020-02-19 03:22:26.000000', null, null, 35030209, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (282, '2020-02-19 03:22:26.000000', null, null, 35030210, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (283, '2020-02-19 03:22:26.000000', null, null, 35030211, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (284, '2020-02-19 03:22:26.000000', null, null, 35030212, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (285, '2020-02-19 03:22:26.000000', null, null, 35030213, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (286, '2020-02-19 03:22:26.000000', null, null, 35030214, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (287, '2020-02-19 03:22:26.000000', null, null, 35030215, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (288, '2020-02-19 03:22:26.000000', null, null, 35030216, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (289, '2020-02-19 03:22:26.000000', null, null, 35030217, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (290, '2020-02-19 03:22:26.000000', null, null, 35030218, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (291, '2020-02-19 03:22:26.000000', null, null, 35030219, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (292, '2020-02-19 03:22:26.000000', null, null, 35030220, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (293, '2020-02-19 03:22:26.000000', null, null, 35030221, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (294, '2020-02-19 03:22:26.000000', null, null, 35030222, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (295, '2020-02-19 03:22:26.000000', null, null, 35030223, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (296, '2020-02-19 03:22:26.000000', null, null, 35030224, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (297, '2020-02-19 03:22:26.000000', null, null, 35030225, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (298, '2020-02-19 03:22:26.000000', null, null, 35030226, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (300, '2020-02-19 03:22:26.000000', null, null, 35030228, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (301, '2020-02-19 03:22:26.000000', null, null, 35030229, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (302, '2020-02-19 03:22:26.000000', null, null, 35030230, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (303, '2020-02-19 03:22:26.000000', null, null, 35030231, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (304, '2020-02-19 03:22:26.000000', null, null, 35030232, 20);
INSERT INTO public.apply_type_division_rel (id, updatetime, datarole_1, datarole_2, division_id, apply_type_id) VALUES (305, '2020-02-19 03:22:26.000000', null, null, 35030233, 20);

